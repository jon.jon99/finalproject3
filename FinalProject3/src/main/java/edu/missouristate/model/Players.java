package edu.missouristate.model;

public class Players {
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer number;
	private String sportPlayed;
	

	public Players() {
		// TODO Auto-generated constructor stub
	}


	public Players(Integer id, String firstName, String lastName, Integer number, String sportPlayed) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.number = number;
		this.sportPlayed = sportPlayed;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Integer getNumber() {
		return number;
	}


	public void setNumber(Integer number) {
		this.number = number;
	}


	public String getSportPlayed() {
		return sportPlayed;
	}


	public void setSportPlayed(String sportPlayed) {
		this.sportPlayed = sportPlayed;
	}


	
}
